<section id="the-event" class="section align-center">
    <div class="container">

        <?php if( have_rows('sponsors') ): $sponsorCount = 0; ?>
        <?php if(get_field('sponsors_title')): ?><h5 class="h7"><?php the_field('sponsors_title'); ?></h5><?php endif; ?>

          <div class="sponsor-carousel">
            <?php while ( have_rows('sponsors') ) : the_row(); ?>
            <?php if(get_sub_field('url')): ?>
            <div><a href="<?php the_sub_field('url'); ?>" target="_blank"><img src="<?php the_sub_field('logo'); ?>" alt="<?php the_sub_field('title'); ?>" /></a></div>
            <?php else: ?>
            <div><img src="<?php the_sub_field('logo'); ?>" alt="<?php the_sub_field('title'); ?>" /></div>
            <?php endif; ?>
            <?php endwhile; ?>
          </div>

        <?php endif; ?>

        <?php if(get_field('event_image')): ?><img src<?php the_field('event_image'); ?></h5><?php endif; ?>

        <?php if(get_field('event_subheading')): ?><h5 class="uppercase"><?php the_field('event_subheading'); ?></h5><?php endif; ?>

        <?php if(get_field('event_description_heading')): ?><h1><?php the_field('event_description_heading'); ?></h1><?php endif; ?>
        <div class="carousel-wrap">
          <div id="sponsorsControls" class="carousel slide" data-ride="carousel" data-interval="5000">
            <ol class="carousel-indicators">
                <li data-target=".carousel" data-slide-to="0" class="active"></li>
                <li data-target=".carousel" data-slide-to="1"></li>
                <li data-target=".carousel" data-slide-to="2"></li>
                <li data-target=".carousel" data-slide-to="3"></li>
                <li data-target=".carousel" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">
              <div class="item active">
                <img src="/media/CIA-partnership-1170x310.jpg" alt="partnership">
              </div>
              <div class="item">
                <img src="/media/CIA-goldsponsor-1170x310.jpg" alt="gold-sponsors">
              </div>
              <div class="item">
                <img src="/media/CIA-silversponsor-1170x310.jpg" alt="silver-sponsor">
              </div>
              <div class="item">
                <img src="/media/CIA-bronzesponsor-1170x310.jpg" alt="bronze-sponsors">
              </div>
              <div class="item">
                <img src="/media/CIA-media-1170x310.jpg" alt="media-partners">
              </div>      
            </div>
          </div>
        </div><!--End of Carousel Wrap-->
        <?php if(get_field('event_description_content')): ?>
        <div class="event-description">
          <div class="row">
            <div class="col-sm-12"><?php the_field('event_description_content'); ?></div>
          </div>
        </div>
		    <?php endif; ?>

        <?php if(get_field('event_heading')): ?><h1><?php the_field('event_heading'); ?></h1><?php endif; ?>
        <div>
			<?php if(!get_field('event_right_column')): ?>
            <div class="col-sm-12">
                <?php the_field('event_left_column'); ?>
            </div>
            <?php else: ?>
            <div class="col-sm-6">
                <?php the_field('event_left_column'); ?>
            </div>
            <div class="col-sm-6">
                <?php the_field('event_right_column'); ?>
            </div>
        </div>
		<?php endif; ?>
    </div>

</section>

<div id="signup-top" class="signup-top section overlay bg-cover" style="background-image: url('/media/banner-register.jpg')">
  <div class="container">
    <a class="signup-link" href="https://signup.e2ma.net/signup/1882735/1753370/" onclick="window.open('https://signup.e2ma.net/signup/1882735/1753370/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, height=500'); return false;">Sign up for Updates</a>
  </div>
</div>

<?php
	$montageImages = get_field('photomontage');
	if( $montageImages ):
?>
 <div class="magicwall magicwall-event">
	<ul class="magicwall-grid">
		<?php foreach( $montageImages as $image ): ?>
		<li data-thumb="<?php echo $image['sizes']['montage-photo']; ?>"></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<section id="more-info" class="section align-center">
    <div class="container">

		    <?php if(get_field('event_additional_left') || get_field('event_additional_right')): ?>
        <div>
			      <?php if(!get_field('event_additional_right') && get_field('event_additional_right')): ?>
            <div class="col-sm-12">
                <?php if(get_field('event_additional_title_left')): ?>
                <h5><?php if(get_field('event_additional_icon_left')) the_field('event_additional_icon_left'); ?><?php the_field('event_additional_title_left'); ?></h5>
                <?php endif; ?>
                <?php the_field('event_additional_left'); ?>
            </div>
            <?php else: ?>
            <div class="col-sm-6">
                <?php if(get_field('event_additional_title_left')): ?>
                <h5><?php if(get_field('event_additional_icon_left')) the_field('event_additional_icon_left'); ?><?php the_field('event_additional_title_left'); ?></h5>
                <?php endif; ?>
                <?php the_field('event_additional_left'); ?>
            </div>
            <div class="col-sm-6">
                <?php if(get_field('event_additional_title_right')): ?>
                <h5><?php if(get_field('event_additional_icon_right')) the_field('event_additional_icon_right'); ?><?php the_field('event_additional_title_right'); ?></h5>
                <?php endif; ?>
                <?php the_field('event_additional_right'); ?>
            </div>
        </div>
		    <?php endif; endif; ?>

        <div class="even-more accred-callout col-sm-12">
          <?php the_field('accreditation_block', false, false); ?>
        </div>

    </div>
</section>



<?php if( have_rows('speakers') || have_rows('keynote_speakers') ): $speakerCount = 0; $totalSpeakers = count(get_field('speakers')); ?>
<section id="speakers" class="align-center">

    <?php if(get_field('speakers_heading')): ?>
    <div id="speaker-header" class="section overlay bg-cover light-text"<?php if(get_field('speakers_heading_background')): ?> style="background-image: url('<?php the_field('speakers_heading_background'); ?>')"<?php endif; ?>>
        <div class="container">
            <h1><?php the_field('speakers_heading'); ?></h1>
        </div>
    </div>
    <?php endif; ?>

    <?php if( have_rows('keynote_speakers')): ?>
    <div id="keynote-speaker-info" class="section">
      <div class="container">
        <div class="row">

         <h4 style="font-weight: 500; padding-bottom: 30px;">See who joined us last year</h4> 

        <?php if(get_field('keynote_speakers_heading')): ?><h5><?php the_field('keynote_speakers_heading'); ?></h5><?php endif; ?>

        <?php while ( have_rows('keynote_speakers') ) : the_row();
          $speakerPhoto = get_sub_field('photo');
          $speakerPhotoSize = 'thumbnail';
          $speakerPhotoThumb = $speakerPhoto['sizes'][ $speakerPhotoSize ];
          if ($speakerCount !== 0 && $speakerCount % 3 === 0):
        ?>
        </div>
        <div class="row">
        <?php endif; ?>
          <div class="col-sm-4">
            <div class="speaker">
              <div class="photo-wrapper rounded"><img src="<?php echo $speakerPhotoThumb; ?>" alt="<?php the_sub_field('name'); ?>" class="img-responsive"></div>
              <h3 class="name"><?php the_sub_field('name'); ?></h3>
              <?php if(get_sub_field('title')): ?><p class="text-alt"><small><?php the_sub_field('title'); ?></small></p><?php endif; ?>
              <p class="about"><?php the_sub_field('about'); ?></p>
              <?php if(get_sub_field('additional_bio')): ?><button type="button" class="btn" data-toggle="modal" data-target="#speaker<?php echo $speakerCount; ?>">Read Bio</button><?php endif; ?>
              </div>
            </div>   

        <?php $speakerCount++; endwhile; $speakerCount = 0; ?>

        </div>

      </div>
    </div>
    <?php endif; ?>

    <?php if( have_rows('speakers')): ?>
    <div id="speaker-info" class="section">
      <div class="container">
			  <div class="row">

        <?php if(get_field('featured_speakers_heading')): ?><h5><?php the_field('featured_speakers_heading'); ?></h5><?php endif; ?>

				<?php while ( have_rows('speakers') ) : the_row();
					$speakerPhoto = get_sub_field('photo');
					$speakerPhotoSize = 'thumbnail';
					$speakerPhotoThumb = $speakerPhoto['sizes'][ $speakerPhotoSize ];
					//Section into groups of 3
					if ($speakerCount !== 0 && $speakerCount % 3 === 0):
				?>
        </div>
        <div class="row">
        <?php endif; ?>

          <div class="<?php if($totalSpeakers == 1) { echo 'col-sm-12'; } else { echo 'col-sm-4'; } ?>">
              <div class="speaker featured-speaker-<?php echo $speakerCount; ?>">
                  <div class="photo-wrapper rounded"><img src="<?php echo $speakerPhotoThumb; ?>" alt="<?php the_sub_field('name'); ?>" class="img-responsive"></div>
                  <h3 class="name"><?php the_sub_field('name'); ?></h3>
                  <?php if(get_sub_field('title')): ?><p class="text-alt"><small><?php the_sub_field('title'); ?></small></p><?php endif; ?>
                  <p class="about"><?php the_sub_field('about'); ?></p>
                  <?php if(get_sub_field('additional_bio')): ?><button type="button" class="btn" data-toggle="modal" data-target="#speaker<?php echo $speakerCount; ?>">Read Bio</button><?php endif; ?>
              </div>
          </div>

				<?php $speakerCount++; endwhile; ?>

			  </div>

        <?php if(get_field('speakers_info')): ?>
        <div class="row">
          <div class="col-sm-12">
            <?php the_field('speakers_info'); ?>
          </div>
        </div>
        <?php endif; ?>

      </div>
    </div>
    <?php endif; ?>

<?php
	if( have_rows('keynote_speakers') ): $speakerCount = 0;
	while ( have_rows('keynote_speakers') ) : the_row();
	$speakerPhoto = get_sub_field('photo');
	$speakerPhotoSize = 'thumbnail';
	$speakerPhotoThumb = $speakerPhoto['sizes'][ $speakerPhotoSize ];
?>
  <?php if(get_sub_field('additional_bio')): ?>
  <div class="modal speaker" id="speaker<?php echo $speakerCount; ?>" tabindex="-1" role="dialog" aria-labelledby="speaker<?php echo $speakerCount; ?>Label">
    <div class="vertical-alignment-helper">
      <div class="modal-dialog vertical-align-center modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="speaker<?php echo $speakerCount; ?>Label"><?php the_sub_field('name'); ?></h4>
          </div>
          <div class="modal-body">
            <div class="photo-wrapper rounded"><img src="<?php echo $speakerPhotoThumb; ?>" alt="<?php the_sub_field('name'); ?>" class="img-responsive"></div>
            <?php the_sub_field('bio'); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>
  <?php $speakerCount++; endwhile; endif; ?>

	<?php
		if( have_rows('speakers') ): $speakerCount = 0;
		while ( have_rows('speakers') ) : the_row();
		$speakerPhoto = get_sub_field('photo');
		$speakerPhotoSize = 'thumbnail';
		$speakerPhotoThumb = $speakerPhoto['sizes'][ $speakerPhotoSize ];
	?>
    <?php if(get_sub_field('additional_bio')): ?>
    <div class="modal speaker" id="speaker<?php echo $speakerCount; ?>" tabindex="-1" role="dialog" aria-labelledby="speaker<?php echo $speakerCount; ?>Label">
      <div class="vertical-alignment-helper">
        <div class="modal-dialog vertical-align-center modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="speaker<?php echo $speakerCount; ?>Label"><?php the_sub_field('name'); ?></h4>
            </div>
            <div class="modal-body">
              <div class="photo-wrapper rounded"><img src="<?php echo $speakerPhotoThumb; ?>" alt="<?php the_sub_field('name'); ?>" class="img-responsive"></div>
              <?php the_sub_field('bio'); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    <?php $speakerCount++; endwhile; endif; ?>

</section>
<?php endif; ?>

<?php if(get_field('schedule_content')): ?>
<section id="schedule" class="align-center">

    <?php if(get_field('schedule_heading')): ?>
    <div id="schedule-header" class="section overlay bg-cover light-text"<?php if(get_field('schedule_heading_background')): ?> style="background-image: url('<?php the_field('schedule_heading_background'); ?>')"<?php endif; ?>>
        <div class="container">
            <h1><?php the_field('schedule_heading'); ?></h1>
            <h5><?php the_field('schedule_subheading'); ?></h5>
        </div>
    </div>
    <?php endif; ?>

    <div id="schedule-info" class="section">
      <div class="container">
			  <div class="row">
          <div class="col-sm-12">
            <?php the_field('schedule_content'); ?>
          </div>
        </div>
      </div>
    </div>

    <?php if( have_rows('schedule') ): $scheduleCount = 1; ?>
    <div class="schedule" class="section">
      <div class="container">

        <div class="nav-center">
          <ul id="schedule-tabs" role="tablist" class="nav-pills">
            <?php
              while ( have_rows('schedule') ) : the_row();
              // $date = get_sub_field('date', false, false);
              // $date = new DateTime($date);
            ?>
            <li<?php if($scheduleCount == 1) echo ' class="active"'; ?>><a data-toggle="tab" href="#day<?php echo $scheduleCount; ?>"><?php echo get_sub_field('date'); ?></a></li>
            <?php $scheduleCount++; endwhile; $scheduleCount = 1; ?>
          </ul>
        </div>

        <div class="static-schedule-text">
          <?php the_field('static_schedule_top'); ?>
        </div>

        <div class="tab-content schedule-content-tab">
          <?php while ( have_rows('schedule') ) : the_row(); ?>
          <div class="tab-pane<?php if($scheduleCount == 1) echo ' active'; ?>" id="day<?php echo $scheduleCount; ?>" role="tabpanel">
            <section class="timeline">

              <?php
                if( have_rows('events') ): $eventCount = 1;
                while ( have_rows('events') ) : the_row();
              ?>
              <?php if($eventCount % 2 == 0): ?>
              <div class="timeline-block">
                <div class="timeline-bullet"></div>
                <div class="timeline-content">

                <?php if(get_sub_field('modal_description')): ?>
                  <h2><a href="<?php bloginfo('url'); ?>/schedule/#event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"  title="Event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"><?php the_sub_field('title'); ?><i class="fa fa-info-circle" aria-hidden="true"></i></a></h2> 
            <?php else: ?>
              <h2><?php the_sub_field('title'); ?></h2>    
            <?php endif; ?> 
                  <?php if(get_sub_field('description')): ?><p><?php the_sub_field('description'); ?></p><?php endif; ?>
                  <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_sub_field('time'); ?></span>
                </div><!--End of timeline-content-->
              </div><!--End of timeline-block-->

              <?php else: ?>
              <div class="timeline-block">
                <div class="timeline-bullet"></div>
                <div class="timeline-content">
                  <?php if(get_sub_field('modal_description')): ?>
                  <h2><a href="<?php bloginfo('url'); ?>/schedule/#event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"  title="Event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"><?php the_sub_field('title'); ?><i class="fa fa-info-circle" aria-hidden="true"></i></a></h2>
            <?php else: ?>
              <h2><?php the_sub_field('title'); ?></h2>    
            <?php endif; ?>
          <?php if(get_sub_field('description')): ?><p><?php the_sub_field('description'); ?></p><?php endif; ?>
                  <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_sub_field('time'); ?></span>
                </div><!--End of timeline-content-->
              </div><!--End of timeline-block-->
              <?php endif; ?>
              <?php $eventCount++; endwhile; endif; ?>


            </section>
          </div>
          <?php $scheduleCount++; endwhile; ?>
        </div>

        <div id="legend">
          <div class="row">
            <!--Section 1-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="keynote" data-content="<?php the_field('keynote_text'); ?>" data-placement="top" data-trigger="hover" container="body">
                <div class="keynote-legend">
                </div>
                <p><?php the_field('keynote_title'); ?></p>
              </a>  
            </div>
            <!--Section 2-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="innovation" data-content="<?php the_field('il_text'); ?>" rel="popover" data-placement="top" data-trigger="hover" container="body">
                <div class="innovation-legend">
                </div>
                <p><?php the_field('il_title'); ?></p>
              </a>
            </div>
            <!--Section 3-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="workshop" data-content="<?php the_field('workshop_text'); ?>" rel="popover" data-placement="top" data-trigger="hover" container="body">
                <div class="workshop-legend">
                </div>
                <p><?php the_field('workshop_title'); ?></p>
              </a>
            </div>
            <!--Section 4-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="cbd" data-content="<?php the_field('cbd_text'); ?>" rel="popover" data-placement="top" data-trigger="hover" container="body">
                <div class="cbd-legend">
                </div>
                <p><?php the_field('cbd_title'); ?></p>
              </a>
            </div>
            <!--Section 5-->
            <div class="col-xs-12 col-sm-12 col-md-12">
              <?php the_field('new_legend'); ?>
            </div>
            <!--Section 6-->
            <div class="col-xs-12 col-sm-12 col-md-12 legend-bottom">
              <?php the_field('legend_bottom'); ?>
            </div>
          </div>
        </div>

        <div class="static-schedule-bottom">
          <?php the_field('static_schedule_bottom'); ?>
        </div>
      </div>
    </div>
    <?php endif; ?>

</section>
<?php endif; ?>

<?php
	if((get_field('register_url') || get_field('external_url')) && get_field('register_cta')):
	if(get_field('external_url')) { $registerLink = get_field('external_url'); } else { $registerLink = get_field('register_url');  }
?>
<section id="register" class="signup-bottom section bg-cover overlay light-text align-center" <?php if(get_field('register_banner_background')): ?> style="background-image: url('<?php the_field('register_banner_background'); ?>')"<?php endif; ?>>

    <div class="container">
        <a class="signup-link" href="https://signup.e2ma.net/signup/1882735/1753370/" onclick="window.open('https://signup.e2ma.net/signup/1882735/1753370/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, height=500'); return false;">Sign up for Updates</a>

    </div>
</section>
<?php endif; ?>

<?php if( have_rows('steering_committee') ||  have_rows('cochairs') ||  have_rows('members') ): ?>
<section id="planning-committee" class="section">
	<div class="container">
  <div class="row">
  <div class="col-sm-9 col-sm-offset-2">
      <h5>Planning Committee</h5>
      <?php if(get_field('planning_intro')) the_field('planning_intro'); ?>
    	<?php if(get_field('committee_membership_intro')) the_field('committee_membership_intro'); ?>
        <?php if( have_rows('cochairs')): ?>
        <table class="table table-bordered table-condensed cochairs">
            <thead>
                <tr>
                    <th colspan="2" style="text-align: center;">Co-Chairs</th>
                </tr>
            </thead>
            <tbody>
            	<?php while ( have_rows('cochairs') ) : the_row(); ?>
                <tr>
                    <td class="name col-xs-5 col-sm-4"><?php if(get_sub_field('url')): ?><a href="<?php the_sub_field('url'); ?>" target="_blank"><?php the_sub_field('name'); ?></a><?php else: the_sub_field('name'); endif; ?></td>
                    <td class="title col-xs-7 col-sm-8"><?php the_sub_field('title'); ?></td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        <?php endif; ?>

        <?php if( have_rows('members')): ?>
        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="2" style="text-align: center;">Members</th>
                </tr>
            </thead>
            <tbody>
            	<?php while ( have_rows('members') ) : the_row(); ?>
                <tr>
                    <td class="name col-xs-5 col-sm-4"><?php if(get_sub_field('url')): ?><a href="<?php the_sub_field('url'); ?>" target="_blank"><?php the_sub_field('name'); ?></a><?php else: the_sub_field('name'); endif; ?></td>
                    <td class="title col-xs-7 col-sm-8"><?php the_sub_field('title'); ?></td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        <?php endif; ?>

        <?php if( have_rows('steering_committee')): ?>
        <table class="table table-bordered table-condensed">
            <thead>
                <tr>
                    <th colspan="2" style="text-align: center;"><?php the_field('steering_committee_title'); ?></th>
                </tr>
            </thead>
            <tbody>
            	<?php while ( have_rows('steering_committee') ) : the_row(); ?>
                <tr>
                    <td class="name col-xs-5 col-sm-4"><?php if(get_sub_field('url')): ?><a href="<?php the_sub_field('url'); ?>" target="_blank"><?php the_sub_field('name'); ?></a><?php else: the_sub_field('name'); endif; ?></td>
                    <td class="title col-xs-7 col-sm-8"><?php the_sub_field('title'); ?></td>
                </tr>
                <?php endwhile; ?>
            </tbody>
        </table>
        <?php endif; ?>
    </div>
    </div>
    </div>
</section>
<?php endif; ?>

<section id="sponsors" class="section align-center">
  <div class="container">
      <!--New PARTNERSHIP DIV-->
      <?php if( have_rows('partnership_list') || get_field('partnership_list') ): ?>
        <div class="partnership-bottom">
          <div class="row">
            <div class="col-sm-12 col-xs-12">
              <?php if(get_field('partnership_intro')) the_field('partnership_intro'); ?>
            </div>
            <?php
              if( have_rows('partnership_list') ): $partnershipList = 1;
              while ( have_rows('partnership_list') ) : the_row();
              $partnershipLogo = get_sub_field('logo');
              $partnershipTitle = $partnershipLogo['title'];
              $partnershipLogoSize = 'medium';
              $partnershipLogoThumb = $partnershipLogo['sizes'][ $partnershipLogoSize ];
            ?>
            <div class="col-sm-3">
              <img class="img-responsive" src="<?php echo $partnershipLogoThumb; ?>" alt="<?php echo $partnershipTitle; ?>">
            </div>
            <?php $partnershipList++;endwhile; endif; ?>
          </div>
        </div>
      <?php endif; ?>  
      <!--New PARTNERSHIP DIV End-->
      <!--New Sponsors Start-->
      <?php if( have_rows('sponsors_group') || get_field('sponsors_group') ): $sponsorsGroupCount = 1; ?>
        <?php while( have_rows('sponsors_group')) : the_row(); ?>
        <div class="actual-sponsors">
          <h5 class="sponsors-align"><?php the_sub_field('sponsor_title'); ?></h5>
          <div class="row">
            <?php
              if( have_rows('sponsors') ): $sponsorList = 1;
              while ( have_rows('sponsors') ) : the_row();
              $groupSponsorLogo = get_sub_field('logo');
              $groupSponsorLogoTitle = $groupSponsorLogo['title'];
              $groupSponsorLogoSize = 'medium';
              $groupSponsorLogoThumb = $groupSponsorLogo['sizes'][ $groupSponsorLogoSize ];

              if($sponsorsGroupCount != 2):
            ?>
                <div class="<?php if($sponsorList == 1) echo 'col-sm-offset-3'?> col-sm-3 col-xs-12"><img class="img-responsive" src="<?php echo $groupSponsorLogoThumb; ?>" alt="<?php $groupSponsorLogoTitle; ?>"></div>
              <?php else: ?>
                <div class="col-sm-12 col-xs-12"><img class="img-responsive" src="<?php echo $groupSponsorLogoThumb; ?>" alt="<?php $groupSponsorLogoTitle; ?>"></div>
              <?php endif; ?>   
            <?php $sponsorList++;endwhile; endif; ?>
          </div>
        </div>
        <?php $sponsorsGroupCount++; endwhile; ?>
        <?php if(get_field('sponsors_bottom_text')) the_field('sponsors_bottom_text'); ?>
        <?php endif; ?>
      <!--New Sponsors End-->  
      <!--New Media Sponsor DIV-->
      <div class="media-sponsor">
        <div class="row">
          <div class="col-sm-12 col-xs-12">
            <?php if(get_field('media_sponsor_intro')) the_field('media_sponsor_intro'); ?>
          </div>
          <?php
            if( get_field('media_sponsor_logo') ):
            $mediaSponsorLogo = get_field('media_sponsor_logo');
            $mediaSponsorTitle = $mediaSponsorLogo['title'];
            $mediaSponsorLogoSize = 'medium';
            $mediaSponsorLogoThumb = $mediaSponsorLogo['sizes'][ $mediaSponsorLogoSize ];
          ?>
          <div class="col-sm-12 col-xs-12">
            <img class="img-responsive" src="<?php echo $mediaSponsorLogoThumb; ?>" alt="<?php echo $mediaSponsorTitle; ?>">
          </div>
          <?php endif; ?>
        </div>
      </div>
      <!--New Media Sponsor DIV End-->

      <?php if( have_rows('nbm_groups') || get_field('nbm_groups') ): ?>
      <?php if(get_field('nbm_intro')) the_field('nbm_intro'); ?>  
        <?php if(get_field('nbm_list_type') == 'image-list'): $nbmGroupCount = 1; ?>
        <?php while ( have_rows('nbm_groups') ) : the_row(); ?>
        <div class="sponsors">
            <h5 class="sp-title-<?php echo $nbmGroupCount; ?> <?php if($nbmGroupCount % 2 === 0) echo 'right'; ?>"><span><?php the_sub_field('group_title'); ?></span></h5>
            <div class="row">
        <?php
          if( have_rows('group_members') ):
          while ( have_rows('group_members') ) : the_row();
          $groupNBMLogo = get_sub_field('logo');
          $groupNBMLogoSize = 'medium';
          $groupNBMLogoThumb = $groupNBMLogo['sizes'][ $groupNBMLogoSize ];
        ?>
                <?php if(get_sub_field('url')): ?>
                <div class="col-sm-4 col-xs-12"><a href="<?php the_sub_field('url'); ?>" target="_blank"><img class="img-responsive" src="<?php echo $groupNBMLogoThumb; ?>" alt="<?php the_sub_field('title'); ?>"></a></div>
                <?php else: ?>
                <div class="col-sm-4 col-xs-6"><img class="img-responsive" src="<?php echo $groupNBMLogoThumb; ?>" alt="<?php the_sub_field('title'); ?>"></div>
                <?php endif; ?>
                <?php endwhile; endif; ?>

            </div>
        </div>
        <?php $nbmGroupCount++; endwhile; ?>
        <div class="sponsors">
          <?php the_field('nbm_leaders'); ?>
        </div>
        <?php else: ?>
        <div class="sponsors-list">
          <?php the_field('nbm_list'); ?>
        </div>
        <?php endif; ?>
        <?php if(get_field('sponsor_contacts')): ?><div><?php the_field('sponsor_contacts'); ?></div><?php endif; ?>
    </div>
</section>
<?php endif; ?>

<?php if(get_field('about_content')): ?>
<section id="about" class="align-center">

	<?php if(get_field('about_heading')): ?>
    <div id="about-header" class="section overlay bg-cover light-text"<?php if(get_field('about_heading_background')): ?> style="background-image: url('<?php the_field('about_heading_background'); ?>')"<?php endif; ?>>
        <div class="container">
            <h1><?php the_field('about_heading'); ?></h1>
        </div>
    </div>
    <?php endif; ?>

    <div id="about-info" class="section">
    	<div class="container">
  			<div class="row">
          <div class="col-sm-12">
         		<?php the_field('about_content') ?>
          </div>
        </div>
        </div>
    </div>

    <?php
		$montageImages = get_field('photomontage');
		if( $montageImages ):
	?>
     <div class="magicwall magicwall-about">
        <ul class="magicwall-grid">
        	<?php foreach( $montageImages as $image ): ?>
            <li data-thumb="<?php echo $image['sizes']['montage-photo']; ?>"></li>
            <?php endforeach; ?>
        </ul>
    </div>
    <?php endif; ?>

</section>
<?php endif; ?>

<?php if(get_field('facebook', 'option') || get_field('instagram', 'option') || get_field('twitter', 'option') || get_field('linkedin', 'option') || get_field('youtube', 'option')): ?>
<section id="connect" class="section align-center">
    <div class="container">
        <h5>Connect with Us</h5>
        <ul>
            <?php if(get_field('facebook', 'option')): ?><li><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook fa-fw fa-2x" aria-hidden="true"></i></a></li><?php endif; ?>
            <?php if(get_field('instagram', 'option')): ?><li><a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram fa-fw fa-2x" aria-hidden="true"></i></a></li><?php endif; ?>
            <?php if(get_field('twitter', 'option')): ?><li><a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter fa-fw fa-2x" aria-hidden="true"></i></a></li><?php endif; ?>
            <?php if(get_field('linkedin', 'option')): ?><li><a href="<?php the_field('linkedin', 'option'); ?>" target="_blank"><i class="fa fa-linkedin fa-fw fa-2x" aria-hidden="true"></i></a></li><?php endif; ?>
            <?php if(get_field('youtube', 'option')): ?><li><a href="<?php the_field('youtube', 'option'); ?>" target="_blank"><i class="fa fa-youtube fa-fw fa-2x" aria-hidden="true"></i></a></li><?php endif; ?>
        </ul>
        <?php if(get_field('connect_content')): ?><div><?php the_field('connect_content'); ?></div><?php endif; ?>
    </div>
</section>
<?php endif; ?>

<section id="travel">
    <div class="contacts-wrapper">
    	<?php
			$location = get_field('map_location');
			if( !empty($location) ):
		?>
        <div id="contacts-map" class="map" style="height:560px;">
        	<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
        </div>
		<?php endif; ?>
        <div class="container contacts-on-map-container">
            <div class="contacts-on-map">
                <?php if(get_field('contact_box_title')): ?><h4><?php the_field('contact_box_title'); ?></h4><?php endif; ?>
                <?php if(get_field('location_name')): ?><h5><?php the_field('location_name'); ?></h5><?php endif; ?>
                <?php if(get_field('address')): ?><p><strong><?php the_field('address'); ?></strong></p><?php endif; ?>

                <?php if(get_field('info')): ?><div class="info"><?php the_field('info'); ?></div><?php endif; ?>

                <?php if(get_field('email')): ?><p><strong><a href="mailto <?php the_field('email'); ?>"><?php the_field('email'); ?></a></strong></p><?php endif; ?>
                <?php if(get_field('phone')): ?><p><strong><?php the_field('phone'); ?></strong></p><?php endif; ?>

            </div>
        </div>
    </div>
</section>