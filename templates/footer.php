<footer class="footer align-center">
    <div class="container">
      <div class="row">
          <div class="col-md-12 address cf">
              <ul>
                  <?php if(get_field('footer_logo', 'option')): ?><li><img alt="The Schwartz Center for Compassionate Healthcare" src="<?php the_field('footer_logo', 'option') ?>"><?php endif; ?>
                  <?php if(get_field('copyright', 'option')): ?></li><li><?php the_field('copyright', 'option') ?></li><?php endif; ?>
                  <?php if(get_field('address_1', 'option')): ?><li><?php the_field('address_1', 'option') ?></li><?php endif; ?>
                  <?php if(get_field('address_2', 'option')): ?><li><?php the_field('address_2', 'option') ?></li><?php endif; ?>
              </ul>
          </div>
          <?php if(get_field('facebook', 'option') || get_field('instagram', 'option') || get_field('twitter', 'option') || get_field('linkedin', 'option') || get_field('youtube', 'option')): ?>

          <div class="col-md-12 social">
              <ul>
                  <?php if(get_field('facebook', 'option')): ?><li><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><i class="fa fa-facebook-square fa-2x"></i></a></li><?php endif; ?>
                  <?php if(get_field('instagram', 'option')): ?><li><a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><i class="fa fa-instagram fa-2x"></i></a></li><?php endif; ?>
                  <?php if(get_field('twitter', 'option')): ?><li><a href="<?php the_field('youtube', 'option'); ?>" target="_blank"><i class="fa fa-youtube-square fa-2x"></i></a></li><?php endif; ?>
                  <?php if(get_field('linkedin', 'option')): ?><li><a href="<?php the_field('linkedin', 'option'); ?>" target="_blank"><i class="fa fa-linkedin-square fa-2x"></i></a></li><?php endif; ?>
                  <?php if(get_field('youtube', 'option')): ?><li><a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><i class="fa fa-twitter-square fa-2x"></i></a></li><?php endif; ?>
              </ul>
          </div>
          <?php endif; ?>
          <div class="col-md-12 contact cf">
              <ul> 
                  <?php if(get_field('phone', 'option')): ?><li><i class="fa fa-phone"></i><?php the_field('phone', 'option'); ?></li><?php endif; ?>
                  <?php if(get_field('fax', 'option')): ?><li><i class="fa fa-fax"></i><?php the_field('fax', 'option'); ?></li><?php endif; ?>
                  <?php if(get_field('email', 'option')): ?><li><i class="fa fa-envelope"></i><a href="mailto: <?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a></li><?php endif; ?>
              </ul>
              <div class="newsletter"><a onclick="window.open('https://app.e2ma.net/app2/audience/signup/1795668/1753370/', 'signup', 'menubar=no, location=no, toolbar=no, scrollbars=yes, width=600, height=400'); return false;" href="https://app.e2ma.net/app2/audience/signup/1795668/1753370/" class="btn btn-default"><i class="fa fa-envelope"></i> Sign Up for our Newsletter</a></div>
          </div>
          <div class="col-md-12 navigation cf">
              <nav role="navigation" class="nav-footer">
                <div class="menu-footer-navigation-container">
                      <?php
					  if (has_nav_menu('footer_navigation')) :
						wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav-footer']);
					  endif;
					  ?>
                </div>
            </nav>
          </div>
      </div>
    </div>
</footer>
