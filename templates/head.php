<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta property="og:title" content="The Inaugural Compassion in Action Healthcare Conference | The Schwartz Center for Compassionate Healthcare" />
  <meta property="og:description" content="The Inaugural Compassion in Action Healthcare Conference will bring together clinicians, patients and health system leaders who are committed to making compassion a priority in their organizations and communities. Meet colleagues from across the globe and experience immersive sessions that will leave you inspired, equipped with knowledge and skills and supported by new partners to create and sustain cultures of compassion." />
  <meta property="og:url" content="http://compassioninactionconference.org/" />
  <meta property="og:image" content="http://compassioninactionconference.org/media/150804_Schwartz_Rick_Boyte-7333.jpg" />
  <meta property="og:image" content="http://compassioninactionconference.org/media/150805_Schwartz_Melody_Cunningham-8349.jpg" />
  <meta property="og:image" content="http://compassioninactionconference.org/media/banner-bg.jpg" />
  <?php wp_head(); ?>
</head>
