<div class="container">
<?php the_content(); ?>
	<?php if(is_page("memberday")): ?>
		<?php the_field('top_content'); ?>
		<div class="accordion">
	        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
	        <?php $count = 0; ?>
	        <?php if(have_rows('accordion_segment')):
	        while (have_rows('accordion_segment')) : the_row(); ?>

	        	<div class="panel panel-default">
	                <div class="panel-heading">
	                    <h3 class="panel-title"><a data-toggle="collapse"<?php if($count != 0) echo ' class="collapsed"' ?> data-parent="#accordion" href="#collapse<?php echo $count; ?>"><?php the_sub_field('title'); ?></a></h3>
	                </div>
	                <div id="collapse<?php echo $count; ?>" class="panel-collapse collapse <?php if($count != 0) { echo 'collapsed'; } else { echo 'in'; } ?>">
	                    <div class="panel-body">
	                      <?php the_sub_field('content'); ?>
	                    </div>
	                </div>
	            </div>
	            
	        <?php $count++;endwhile;endif; ?>
	        </div>
	      </div>
	      <?php the_field('bottom_content'); ?>

	<?php endif; ?>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
</div>