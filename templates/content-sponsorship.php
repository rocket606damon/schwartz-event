<div class="container">
<?php the_content(); ?>

<?php if( have_rows('sponsorship_levels') ): $sponsorshipCount = 1; ?>
<div class="sponsorship-levels-content">
  <div class="nav-wrapper">
  <ul class="nav nav-schedule">
    <?php while ( have_rows('sponsorship_levels') ) : the_row(); ?>
    <li<?php if($sponsorshipCount == 1) echo ' class="active"'; ?>><a data-toggle="tab" href="#level<?php echo $sponsorshipCount; ?>"><h5 class="highlight"><?php the_sub_field('title'); ?></h5><p class="text-alt"><?php the_sub_field('price'); ?></p></a></li>
    <?php $sponsorshipCount++; endwhile; $sponsorshipCount = 1; ?>
  </ul>
  </div>

  <div class="tab-content">

    <?php while ( have_rows('sponsorship_levels') ) : the_row(); ?>
    <div class="tab-pane fade in<?php if($sponsorshipCount == 1) echo ' active'; ?>" id="level<?php echo $sponsorshipCount; ?>">
      <?php the_sub_field('content'); ?>
    </div>
    <?php $sponsorshipCount++; endwhile; ?>

  </div>
  <!-- /.tab-content -->
</div>
<div class="disclaimers">
  <?php the_field('disclaimers'); ?>
</div>

<?php endif; ?>
<?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
</div>