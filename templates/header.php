<?php if(is_front_page()): ?>
<section id="hero" class="hero-section bg-cover window-height light-text" style="background-image: url('<?php the_field('banner_image'); ?>')">
    <?php if(get_field('facebook', 'option') || get_field('twitter', 'option') || get_field('linkedin', 'option') || get_field('youtube', 'option')): ?>
    <ul class="socials-nav">
        <?php if(get_field('facebook', 'option')): ?><li class="socials-nav-item"><a href="<?php the_field('facebook', 'option'); ?>" target="_blank"><span class="fa fa-facebook"></span></a></li><?php endif; ?>
        <?php if(get_field('instagram', 'option')): ?><li class="socials-nav-item"><a href="<?php the_field('instagram', 'option'); ?>" target="_blank"><span class="fa fa-instagram"></span></a></li><?php endif; ?>
        <?php if(get_field('twitter', 'option')): ?><li class="socials-nav-item"><a href="<?php the_field('twitter', 'option'); ?>" target="_blank"><span class="fa fa-twitter"></span></a></li><?php endif; ?>
        <?php if(get_field('linkedin', 'option')): ?><li class="socials-nav-item"><a href="<?php the_field('linkedin', 'option'); ?>" target="_blank"><span class="fa fa-linkedin"></span></a></li><?php endif; ?>
        <?php if(get_field('youtube', 'option')): ?><li class="socials-nav-item"><a href="<?php the_field('youtube', 'option'); ?>" target="_blank"><span class="fa fa-youtube-square"></span></a></li><?php endif; ?>
    </ul>
    <?php endif; ?>
    <div class="heading-block centered-block align-center">
        <div class="container">
            <?php if(get_field('top_scch_logo')): ?><img class="top-scch-logo" src="<?php the_field('top_scch_logo') ?>" /><?php endif; ?>
            <?php if(get_field('heading_line_1')): ?><h5 class="heading-alt" style="margin-bottom: 8px;"><?php the_field('heading_line_1') ?></h5><?php endif; ?>
            <?php if(get_field('heading_line_2')): ?><h1 class="extra-heading"><?php the_field('heading_line_2') ?></h1><?php endif; ?>
            <?php if(get_field('heading_line_3')): ?><h5 class="heading-alt"><?php the_field('heading_line_3') ?></h5><?php endif; ?>
            
            <?php /*
            <?php
				if(get_field('cta_link') || get_field('cta_external_link')):
				if(get_field('cta_external_link')) { $ctaLink = get_field('cta_external_link'); } else { $ctaLink = get_field('cta_link');  }
			?>
            <div class="btns-container">
                <a href="<?php echo $ctaLink; ?>" class="btn btn-lg"<?php if(get_field('cta_external_link')) echo ' target="_blank"'; ?>><?php the_field('cta_text') ?></a>
            </div>
            <?php endif; ?>
            <?php */ ?>

            <?php if(get_field('subheading')): ?>
            <style>.heading-alt .more {font-size: 42px;}</style>  
            <h4 class="heading-alt" style="margin: 40px 0; line-height: 40px; display: inline-block; border: 1px solid rgba(254,73,24,0.8); background-color: rgba(254,73,24,0.8); padding: 20px"><?php the_field('subheading') ?></h4>
            <?php endif; ?>
            <?php the_field('top_message'); ?>
        </div>
    </div>
</section>
<?php endif; ?>

<header class="header header-dark">
    <div class="header-wrapper">
        <div class="container">
          <div class="row">
            <div class="col-md-3 col-sm-1 col-xs-12 navigation-header">
                <a href="<?php bloginfo('url'); ?>" class="logo hidden-sm"><?php if(get_field('logo', 'option')): ?><img src="<?php the_field('logo', 'option'); ?>" alt="The Schwartz Center for Compassionate Healthcare"><?php else: ?>The Schwartz Center for Compassionate Healthcare<?php endif; ?></a>
                <a href="<?php bloginfo('url'); ?>" class="logo visible-sm"><?php if(get_field('logo', 'option')): ?><img src="<?php the_field('footer_logo', 'option') ?>" alt="The Schwartz Center for Compassionate Healthcare"><?php else: ?>The Schwartz Center for Compassionate Healthcare<?php endif; ?></a>
                <button class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navigation" aria-expanded="false" aria-controls="navigation">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <div class="col-md-9 col-sm-11 col-xs-12 navigation-container">
                <div id="navigation" class="navbar-collapse collapse">
                    <ul class="navigation-list pull-right light-text">
                        <li class="navigation-item"><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#speakers" class="navigation-link">Speakers</a></li>
                        <li class="navigation-item"><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#schedule" class="navigation-link">Schedule</a></li>
                        <li class="navigation-item"><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>/memberday/" class="navigation-link">Member Day</a></li>
                        <!-- <li class="navigation-item"><a href="<?php //if(!is_front_page()) echo bloginfo('url') ?>#register" class="navigation-link">Register</a></li> -->
                        <!-- <li class="navigation-item"><a href="<?php //if(!is_front_page()) echo bloginfo('url') ?>/poster-submissions/" class="navigation-link">Posters</a></li> -->
                        <li class="navigation-item"><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#sponsors" class="navigation-link">Sponsors</a></li>
                        <li class="navigation-item"><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#about" class="navigation-link">About Us</a></li>
                        <li class="navigation-item"><a href="<?php if(!is_front_page()) echo bloginfo('url') ?>#travel" class="navigation-link">Travel</a></li>
                    </ul>
                </div>
            </div>
        </div>
      </div>
    </div>

    <?php if(is_front_page()): ?>
    <div class="modal" id="welcome-modal" tabindex="-1" role="dialog" aria-labelledby="welcome-modal-Label">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="welcome-modal-label" style="font-size: 28px;">Signup for Updates</h4>
          </div>
          <div class="modal-body">
            <div class="embed-responsive embed-responsive-16by9">
              <iframe src="https://signup.e2ma.net/signup/1882735/1753370/" width="500" height="320" frameborder="0"></iframe>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning nothanks" data-dismiss="modal" style="letter-spacing: .45px;">Please Don't Show This Again</button>
          </div>
        </div>
      </div>
    </div>
    <?php endif; ?>
    
</header>