
    <?php if( have_rows('other_schedule') ): $scheduleCount = 1; ?>
    <div class="schedule" class="section">
      <div class="container">

        <div class="nav-center">
          <ul id="schedule-tabs" role="tablist" class="nav-pills">
            <?php
              while ( have_rows('other_schedule') ) : the_row();
            ?>
            <li<?php if($scheduleCount == 1) echo ' class="active"'; ?>><a data-toggle="tab" href="#day<?php echo $scheduleCount; ?>"><?php echo get_sub_field('date'); ?></a></li>
            <?php $scheduleCount++; endwhile; $scheduleCount = 1; ?>
          </ul>
        </div>

        <div class="tab-content schedule-content-tab">
          <?php while ( have_rows('other_schedule') ) : the_row(); ?>
        	<div class="tab-pane<?php if($scheduleCount == 1) echo ' active'; ?>" id="day<?php echo $scheduleCount; ?>" role="tabpanel">
        		<section class="timeline">

              <?php
                if( have_rows('events') ): $eventCount = 1;
                while ( have_rows('events') ) : the_row();
              ?>
            <?php if($eventCount % 2 == 0): ?>
              <div class="timeline-block">
        				<div class="timeline-bullet"></div>
        				<div class="timeline-content">

                <?php if(get_sub_field('modal_description')): ?>
        					<h2><a href="#event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"  title="Event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"><?php the_sub_field('title'); ?><i class="fa fa-info-circle" aria-hidden="true"></i></a></h2>	
                <?php else: ?>
                  <h2><?php the_sub_field('title'); ?></h2>    
						    <?php endif; ?> 
                  <?php if(get_sub_field('description')): ?><p><?php the_sub_field('description'); ?></p><?php endif; ?>
                  <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_sub_field('time'); ?></span>
        				</div><!--End of timeline-content-->
        			</div><!--End of timeline-block-->

            <?php else: ?>
              <div class="timeline-block">
        				<div class="timeline-bullet"></div>
        				<div class="timeline-content">

                <?php if(get_sub_field('modal_description')): ?>
                  <h2><a href="#event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"  title="Event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"><?php the_sub_field('title'); ?><i class="fa fa-info-circle" aria-hidden="true"></i></a></h2> 
                <?php else: ?>
                  <h2><?php the_sub_field('title'); ?></h2>    
                <?php endif; ?> 
                  <?php if(get_sub_field('description')): ?><p><?php the_sub_field('description'); ?></p><?php endif; ?>
                  <span class="date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?php the_sub_field('time'); ?></span>
                </div><!--End of timeline-content-->
              </div><!--End of timeline-block-->

              <?php endif; ?>

              <?php $eventCount++; endwhile; endif; ?>

        		</section>

        	</div>
          <?php $scheduleCount++; endwhile; ?>
        </div>

        <div id="legend">
          <div class="row">
            <!--Section 1-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="keynote" data-content="<?php the_field('keynote_text'); ?>" rel="popover" data-placement="top" data-trigger="hover" container="body">
                <div class="keynote-legend">
                </div>
                <p><?php the_field('keynote_title'); ?></p>
              </a>  
            </div>
            <!--Section 2-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="innovation" data-content="<?php the_field('il_text'); ?>" rel="popover" data-placement="top" data-trigger="hover" container="body">
                <div class="innovation-legend">
                </div>
                <p><?php the_field('il_title'); ?></p>
              </a>
            </div>
            <!--Section 3-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="workshop" data-content="<?php the_field('workshop_text'); ?>" rel="popover" data-placement="top" data-trigger="hover" container="body">
                <div class="workshop-legend">
                </div>
                <p><?php the_field('workshop_title'); ?></p>
              </a>
            </div>
            <!--Section 4-->
            <div class="col-xs-12 col-sm-3 col-md-3">
              <a id="cbd" data-content="<?php the_field('cbd_text'); ?>" rel="popover" data-placement="top" data-trigger="hover" container="body">
                <div class="cbd-legend">
                </div>
                <p><?php the_field('cbd_title'); ?></p>
              </a>
            </div>
            <!--Section 5-->
            <div class="col-xs-12 col-sm-12 col-md-12">
              <?php the_field('new_legend'); ?>
            </div>
            <!--Section 6-->
            <div class="col-xs-12 col-sm-12 col-md-12 legend-bottom">
              <?php the_field('legend_bottom'); ?>
            </div>
          </div>
        </div>
        
      </div>
    </div>  

    <div id="events">
    <?php $scheduleCount = 1; while ( have_rows('other_schedule') ) : the_row(); 

    	$eventCount = 1; while ( have_rows('events') ) : the_row(); ?>

    	<?php if(get_sub_field('modal_description')): ?>
          <div id="event<?php echo $scheduleCount; ?>_<?php echo $eventCount; ?>"> 
            <div class="events-inner">
              <?php if($scheduleCount . "_" . $eventCount === '1_1'): ?>
                <h2 style="font-size: 60px;">SUNDAY</h2>
              <?php elseif($scheduleCount . "_" . $eventCount === '2_2'): ?>
                <h2 style="font-size: 60px;">MONDAY</h2>
              <?php elseif($scheduleCount . "_" . $eventCount === '3_4'): ?>
                <h2 style="font-size: 60px;">TUESDAY</h2>
              <?php endif; ?> 
              <p class="schedule-time"><strong><i class="fa fa-clock-o" aria-hidden="true"></i><?php the_sub_field('time'); ?></strong></p> 
              <?php if(get_sub_field('title') == 'Keynotes and Conversations' || get_sub_field('title') == 'Case Study' || get_sub_field('title') == 'Plenary Panel Discussion' || get_sub_field('title') == 'Schwartz Center Member Day Welcome and Lunch'): ?>
                <h5 class="keynote-icn"><?php the_sub_field('title'); ?></h5>
              <?php elseif(get_sub_field('title') == 'Compassion by Design'): ?>
                <h5 class="inno-icn" style="background-position-x: 1px!important; padding: 10px 32px;"><?php the_sub_field('title'); ?></h5>  
              <?php else: ?>
                <h5><?php the_sub_field('title'); ?></h5>  
              <?php endif; ?>  
              <?php if(get_sub_field('description')): ?><h4 class="schedule-description"><?php the_sub_field('description'); ?></h4>
              <?php endif; ?>    
              <?php the_sub_field('modal_description'); ?>
            </div>
          </div>
        <?php endif; ?> 

    	<?php $eventCount++; endwhile; 

    $scheduleCount++; endwhile; ?>
    </div>

    <div class="invite-to-other">
      <div class="container">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 legend-bottom">
            <?php the_field('below_schedule_content'); ?>
          </div>
        </div>
      </div>
    </div>  	

        <a href="#0" class="cd-top">Top</a>
    <?php endif; ?>