<!--Header section starts -->
<section id="header-top">
	<div class="banner-content clearfix">
 		<div class="col-sm-12 col-xs-12">        	
           <div class="col-sm-3 col-xs-12">
               <div class="logo pull-left">
                   <a href="http://www.theschwartzcenter.org/" title="The Schwartz Center"><img src="/media/logo-linkedin.png" alt="The Schwartz Center"></a>
               </div>
           </div>
           <div class="col-sm-6 col-xs-12">
               <div class="header-middle">
                    <h1><span>The Annual</span>compassion <br> <em>in</em> action<span>healthcare conference</span></h1>
               </div>
           </div>
	       <div class="col-sm-3 col-xs-12">
               <div class="header-right pull-right">
                    <h5>25-27 june 2017<span> boston, MA</span></h5>
               </div>
           </div>
	    </div>
    </div>
</section>
<!-- Header section ends -->
<!-- Mid content section starts -->
<section class="mid-content">	
   <!-- 	<div class="container"> -->
    	<div class="row disp-tbl">
           <div class="col-sm-7 col-xs-12 col-md-7 disp-tbl-cell">
	           	<div class="mid-content-left">
    	           <h2>Ever ask yourself how <span class="ellipses">...</span> </h2>
                   <ul class="top-list">
	                   	<li>A stronger team-based approach can improve patient experience?</li>
						<li>Compassionate, collaborative care can prevent burnout and enhance well-being?</li>
						<li>My organization can prioritize compassion to help reach quality and safety goals?</li>
                   </ul>
                   <h5>thriving through a Culture of Compassion</h5>
				   <p>Meet colleagues from across the globe and experience immersive sessions that will leave you inspired, equipped with knowledge and skills and supported by new
                    partners to create and sustain cultures of compassion. The Compassion in Action Healthcare Conference’s aim is to inspire and provide the tools and skills 
                    essential to improving patient experience, workforce well-being, quality and safety through compassionate, collaborative care.
				   </p>
				   <p>Going beyond simply highlighting examples of successful initiatives to understand what others have done, participants will learn how to do it where they are.
                   </p>
                   <h6>Keynotes and Conversations</h6>
                       <ul class="list">
                       		<li>CEO perspectives: Leading a culture of compassion</li>
                            <li>Creating and supporting authentic healing relationships</li>
                            <li>Search Inside Yourself: Emotional intelligence lessons from Google</li>
                            <li>The future of compassion in healthcare delivery, payment and policy</li>
                       </ul>
                   <h6>Case-based Discussions, Innovation Labs &amp; Workshops</h6>
                   	  <ul class="list">
                       		<li>Articulating core values and empowering culture change</li>
                            <li>The new high functioning team: Who’s on it and how does it work</li>
                            <li>Fostering intra/inter-personal skills to create authentic healing relationships</li>
                      </ul>
               </div>
           </div>
           <div class="col-sm-5 col-md-5 col-xs-12 disp-tbl-cell">
	        	<div class="mid-content-right">
                	<div class="mid-content-rt">
                        <h6>gain access to the</h6>
                        <h3>Compassion in Action <br/>Webinar Series</h3>
                        <div class="webinar-feedback clearfix">
                            <div class="col-sm-offset-1 col-sm-4">                        	
                                <div class="webinar-img">
                                <img src="/media/evan-ekman.jpg" alt="Evan Ekman">
                                </div>
                            </div>
                            <div class="col-sm-7 webinar-content">                        	
                                <p><strong>“The Alchemy of Empathy and Compassion: Transforming Stress into Meaning at Work”</strong></p>
                                <p>Presented by Eve Ekman, PhD, MSW, postdoctoral student, Osher Center for Integrative Medicine, University of California, San Francisco</p>
                            </div>  
                        </div> 
                        <div class="webinar-feedback clearfix">
                            <div class="col-sm-offset-1 col-sm-4">                        	
                                <div class="webinar-img">
                                <img src="/media/sharon-salzberg.jpg" alt="Evan Ekman">
                                </div>
                            </div>
                            <div class="col-sm-7 webinar-content">                        	
                                <p><strong>“Cultivating Compassion to Avoid Burnout”</strong></p>
                                <p>Presented by Sharon Salzberg, meditation teacher and <i>New York Times</i> best-selling author</p>
                            </div>  
                        </div>
                        <div class="webinar-feedback clearfix">
                            <div class="col-sm-offset-1 col-sm-4">                        	
                                <div class="webinar-img">
                                <img src="/media/katherine.jpg" alt="Evan Ekman">
                                </div>
                            </div>
                            <div class="col-sm-7 webinar-content">                        	
                                <p><strong>“When Emotion Fills the Room: How to use Empathic Statements to Move a Conversation Forward”</strong></p>
                                <p>Presented by Katherine Aragon, MD, Director of Palliative Medicine at Mount Auburn Hospital</p>
                            </div>  
                        </div> 
                     </div>                                      
                </div>
           </div>
           </div>
<!--  </div> -->
</section>
<!-- Mid content section end -->
<!-- Sign up section starts -->
<section class="signup-section">
  <div class="container">
      <div class="row">
          <div class="col-sm-3 col-xs-12">
              <div class="signup-left">
                  <h4>Sign Up</h4>
                </div>
            </div>
            <div class="col-sm-9 col-xs-12">
              <div class="row">
                    <div class="signup-form">
                        <p>Learn more about the conference and gain exclusive access to our Compassion in Action webinar series, which teaches some of the concepts and skills that are essential to providing compassionate, collaborative care  in ways that matter to patients, families and healthcare professionals.</p>

                        <p><sup>*</sup>Email Address Required.</p>
                        <?php echo do_shortcode('[gravityform id=5 title=false description=false ajax=true]'); ?>
                    </div> 
                  </div>               
            </div>
        </div>
    </div>
</section>
<!-- Sign up section ends -->
<!-- Gallery section start -->
<section class="gallery-section">
   	<ul class="clearfix">
    	<li><img src="/media/linkedin_cropped_1.png" alt="linkedin-cropped-1"></li>        
    	<li><img src="/media/linkedin_cropped_2.png" alt="linkedin-cropped-2"></li>        
    	<li><img src="/media/linkedin_cropped_3.png" alt="linkedin-cropped-3"></li>        
    	<li><img src="/media/linkedin_cropped_4.png" alt="linkedin-cropped-4"></li>
       	<li><img src="/media/linkedin_cropped_5.png" alt="linkedin-cropped-5"></li>        
    	<li><img src="/media/linkedin_cropped_6.png" alt="linkedin-cropped-6"></li>
        <li><img src="/media/linkedin_cropped_7.png" alt="linkedin-cropped-7"></li>        
    	<li><img src="/media/linkedin_cropped_8.png" alt="linkedin-cropped-8"></li>        
    	<li><img src="/media/linkedin_cropped_9.png" alt="linkedin-cropped-9"></li>        
    	<li><img src="/media/linkedin_cropped_10.png" alt="linkedin-cropped-10"></li>
       	<li><img src="/media/linkedin_cropped_11.png" alt="linkedin-cropped-11"></li>
   		<li><img src="/media/linkedin_cropped_12.png" alt="linkedin-cropped-12"></li>
    </ul>
</section>
<!-- Gallery section ends -->


<!--Footer section starts-->
<footer id="footer-main" class="clearfix">
  <div class="container">   
  	<div class="row"> 
      <div class="col-sm-12">
           <div class="footer-content">
          	    <a href="http://www.theschwartzcenter.org/" title="The Schwartz Center" class="footer-logo"><img src="/media/footer-logo-linkedin.png" alt="The Schwartz Center"></a>       
           		<p>&copy;2017 The Schwartz Center 100 Cambridge St., Ste. 2100 Boston, Massachusetts 02114</p>
           </div>	
	  </div>
     </div>
  </div>
</footer>
<!--Footer section ends-->