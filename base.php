<?php

use Roots\Sage\Setup;
use Roots\Sage\Wrapper;

?>

<!doctype html>
<html <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if IE]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php if (is_page('learn-more')): 
      get_template_part('template-learn-more');
      elseif (is_page('thank-you')):
      get_template_part('template-linkedin-thank-you');  
    ?>

    <?php else: ?> 

    <?php
      do_action('get_header');
      get_template_part('templates/header');
    ?>
    <div class="wrap<?php if(!is_front_page() && !is_page('other-schedule') && !is_page('schedule')) echo ' container'; ?>" role="document">
      <div class="content row">
        <main class="main">
          <?php include Wrapper\template_path(); ?>
        </main><!-- /.main -->
        <?php if (Setup\display_sidebar()) : ?>
          <aside class="sidebar">
            <?php include Wrapper\sidebar_path(); ?>
          </aside><!-- /.sidebar -->
        <?php endif; ?>
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php
      do_action('get_footer');
      get_template_part('templates/footer');
    ?>

    <?php endif; ?>

    <?php
      wp_footer();
      if(get_field('google_maps_api_key', 'option') && get_field('map_location')) get_template_part('templates/google-map');
    ?>

    <script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/cookie.js"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-89242557-1', 'auto');
      ga('require', 'displayfeatures');
      ga('send', 'pageview');
    </script>

    <script type="text/javascript"> _linkedin_data_partner_id = "34605"; </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script>

  </body>
</html>
