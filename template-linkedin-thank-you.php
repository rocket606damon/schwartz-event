<!-- Thank You section starts -->
<section class="thank-you-section">	
   	<div class="container">
    	<div class="row">
        	<div class="col-sm-12 col-xs-12">
            	<div class="thanks-wrap">
                	<h2>Thank you!</h2>
                    <h5>Thank you for your interest in the Compassion in Action Healthcare Conference. Keep a lookout for email updates with additional information on conference speakers, sessions and other highlights.</h5>
                    <a href="http://compassioninactionconference.org/" title="Register For The Conference" class="btn">register for the conference</a>
                </div>
            </div>
        </div>           
   </div>
</section>
<!-- Thank You section end -->

<!-- Webinar Series section starts -->
<?php if($_GET['email'] != ''): ?>
<section class="webinar-series-section">
	<div class="title">
       	<h6>Compassion in Action Webinar Series</h6>
    </div>	
   	<div class="container">
    	<div class="row">
        	<div class="col-sm-12 col-xs-12">
            	<div class="webinar-wrap">
                	<ul class="clearfix">
                    	<li class="col-sm-4 col-xs-12">
                          <div class="video-thumb">
                            <iframe src="https://www.youtube.com/embed/9Jl6BfKxvG0" frameborder="0" allowfullscreen></iframe>                
                          </div>
                          <div class="video-content" data-mh="video-content">
                             <h6>Eve Ekman, PhD, MSW, postdoctoral student, Osher Center for Integrative Medicine, University of California, San Francisco</h6>
                          </div>
                        </li>
                        <li class="col-sm-4 col-xs-12">
                            <div class="video-thumb">
                              <iframe src="https://www.youtube.com/embed/m_ymcagWmmY" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="video-content" data-mh="video-content">
                                <h6>Sharon Salzberg, meditation teacher and <i>New York Times</i> best-selling author</h6>
                            </div>
                        </li>
                        <li class="col-sm-4 col-xs-12">
                          	<div class="video-thumb">                                	
                              <iframe src="https://www.youtube.com/embed/nDPRDP3_eOY" frameborder="0" allowfullscreen></iframe>
                            </div>
                            <div class="video-content clearfix" data-mh="video-content">
                                <h6>Katherine Aragon, MD, Director of Palliative Medicine at Mount Auburn Hospital</h6>
                            </div>
                        </li>
                    </ul>
                    <div class="webinar-bottom-content">
	                    <h5>Stay tuned for our upcoming Compassion in Action Webinar Series starting January 2017, featuring healthcare leaders who will be participating in the Compassion in Action Healthcare Conference.</h5>
                    </div>
                </div>
            </div>
        </div>           
   </div>
</section>
<?php endif; ?>
<!-- Webinar Series section end -->

<!--Footer section starts-->
<footer id="footer-main">
  <div class="container">   
  	<div class="row"> 
      <div class="col-sm-12">
           <div class="footer-content">
          	    <a href="http://www.theschwartzcenter.org/" title="The Schwartz Center" class="footer-logo"><img src="/media/footer-logo-linkedin.png" alt="The Schwartz Center"></a>       
           		<p>&copy;2017 The Schwartz Center 100 Cambridge St., Ste. 2100 Boston, Massachusetts 02114</p>
           </div>	
	  </div>
     </div>
  </div>
</footer>
<!--Footer section ends-->