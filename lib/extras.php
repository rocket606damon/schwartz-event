<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Change default media folder
 */
update_option('uploads_use_yearmonth_folders', 0);
update_option('upload_path', 'media');

/**
 * SEO Framework - Default OG Image
 */ 

add_filter('the_seo_framework_og_image_args', function($args) {
    $args['image'] = home_url('/media/cappc-og.png');
    return $args;
});

/**
 * SEO Framework - Remove Branding
 */ 
add_filter('the_seo_framework_indicator', '__return_false');

/**
 * SEO Framework - Metabox Priority
 */ 
add_filter('the_seo_framework_metabox_priority', function() {
	return 'low';
});

//Giving Editors Access to Gravity Forms
function add_grav_forms(){
    $role = get_role('editor');
    $role->add_cap('gform_full_access');
}
add_action('admin_init', __NAMESPACE__ . '\\add_grav_forms');

/**
 * Gravity Forms - Hide Labels
 */ 
add_filter( 'gform_enable_field_label_visibility_settings', '__return_true' );

/**
 * Gravity Forms - Init Scripts in the Footer
 */
add_filter( 'gform_init_scripts_footer',  '__return_true' );
add_filter( 'gform_cdata_open', __NAMESPACE__ . '\\wrap_gform_cdata_open', 1 );
function wrap_gform_cdata_open( $content = '' ) {
if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
return $content;
}
$content = 'document.addEventListener( "DOMContentLoaded", function() { ';
return $content;
}
add_filter( 'gform_cdata_close', __NAMESPACE__ . '\\wrap_gform_cdata_close', 99 );
function wrap_gform_cdata_close( $content = '' ) {
if ( ( defined('DOING_AJAX') && DOING_AJAX ) || isset( $_POST['gform_ajax'] ) ) {
return $content;
}
$content = ' }, false );';
return $content;
}

/**
 * Gravity Forms - Change Expiration Date
 */
add_filter( 'gform_incomplete_submissions_expiration_days', __NAMESPACE__ . '\\expiration_date', 1, 10 );

function expiration_date( $expiration_days ) {
    $expiration_days = 90;
    return $expiration_days;
}

/**
* Min/Max Word Count
*/
function count_words($string) {
	$string = strip_tags(html_entity_decode($string, ENT_QUOTES, 'UTF-8'));
 	$splitted = preg_split('/\s+\b/', $string, null, PREG_SPLIT_NO_EMPTY);
 	$counted = count($splitted);
 	foreach($splitted as $i=>$v) {
 		$v = trim($v);
 		$joined = preg_split('#[,;\.?!\/]#', $v, null, PREG_SPLIT_NO_EMPTY);
 		if (count($joined) < 2)
 			continue;
 		if ( empty($joined[0]) || empty($joined[1]) )
 			continue;
 		$joined[1] = trim($joined[1]);
 		//error_log(quoted_printable_encode($joined[1]));
		if ( preg_match('/\w/', $joined[1]) ) {
			$counted++;
		 }
 	}//endforeach
 	return $counted;
}

class GW_Minimum_Characters {

    public function __construct( $args = array() ) {

        // make sure we're running the required minimum version of Gravity Forms
        // if( ! property_exists( 'GFCommon', 'version' ) || ! version_compare( GFCommon::$version, '1.7', '>=' ) )
        //     return;

    	// set our default arguments, parse against the provided arguments, and store for use throughout the class
    	$this->_args = wp_parse_args( $args, array(
    		'form_id' => false,
    		'field_id' => false,
    		'min_chars' => 0,
            'max_chars' => false,
            'validation_message' => false,
            'min_validation_message' => __( 'Please enter at least %s characters.' ),
            'max_validation_message' => __( 'You may only enter %s characters.' )
    	) );

        extract( $this->_args );

        if( ! $form_id || ! $field_id || ! $min_chars )
            return;

    	// time for hooks
    	add_filter( "gform_field_validation_{$form_id}_{$field_id}", array( $this, 'validate_character_count' ), 10, 4 );

    }


    public function validate_character_count( $result, $value, $form, $field ) {

        $char_count = count_words( $value );

        $is_min_reached = $this->_args['min_chars'] !== false && $char_count >= $this->_args['min_chars'];
        $is_max_exceeded = $this->_args['max_chars'] !== false && $char_count > $this->_args['max_chars'];

        if( ! $is_min_reached ) {

            $message = $this->_args['validation_message'];
            if( ! $message )
                $message = $this->_args['min_validation_message'];

            $result['is_valid'] = false;
            $result['message'] = sprintf( $message, $this->_args['min_chars'] );

        } else if( $is_max_exceeded ) {

            $message = $this->_args['max_validation_message'];
            if( ! $message )
                $message = $this->_args['validation_message'];

            $result['is_valid'] = false;
            $result['message'] = sprintf( $message, $this->_args['max_chars'] );

        }

        return $result;
    }

}

# Configuration

// 2016 Schwartz Center IMPACT Honors
new GW_Minimum_Characters( array(
    'form_id' => 4,
    'field_id' => 55,
    'min_chars' => 250,
    'min_validation_message' => __( 'Oops! You need to enter at least %s words.' )
) );

/**
 * Gravity Forms - Word Count Script
 */
function els_load_scripts() {
	wp_enqueue_script('gravity-forms-word-count', get_stylesheet_directory_uri() . '/dist/scripts/wordcount-42bd4b9208.js', array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\els_load_scripts');

/**
 * Enqueue Back to Top Script
 */
function back_to_top() {
    wp_enqueue_script('back_to_top', get_stylesheet_directory_uri() . '/dist/scripts/backToTop-a89d4d3c4d.js', array('jquery'), null, true);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\back_to_top');

/**
 * Register ACF Options Page
 */ 
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'General Settings',
		'menu_title'	=> 'General Settings',
		'menu_slug' 	=> 'general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Footer Settings',
		'menu_title'	=> 'Footer Settings',
		'parent_slug'	=> 'general-settings',
	));
	
}

/**
 * Typekit
 */ 
function typekit_inline() {
if ( wp_script_is( 'typekit', 'done' ) ) { ?>
<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
<?php
  }
}
add_action( 'wp_head', __NAMESPACE__ . '\\typekit_inline' );

/**
 * ACF: Temporary fix for Google Maps field that requires API Key
 */
if(function_exists('get_field')){
	if(get_field('google_maps_api_key', 'option')){
		add_filter('acf/settings/google_api_key', function () {
			return get_field('google_maps_api_key', 'option');
		});
	}
}